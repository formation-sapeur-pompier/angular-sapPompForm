import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgentService } from '../services/agent.service';
import { AuthenticationService } from '../services/authentication.service';
import { CandidatureService } from '../services/candidature.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.css']
})
export class AgentComponent implements OnInit {
  isLoggedIn = false;
  user: any;
  mode=0;
  agent:any;
  collapse: boolean = true;
  toggleSideBar: boolean = false
  agentForm: any;
  
  deleteAgentTitle = 'Confirmation';
  deleteAgentMessage = 'Êtes-vous sûr de vouloir supprimer';
  confirmClicked = false;
  cancelClicked = false
  cancelText='Annuler'
  supprimerText='Supprimer'


  constructor(private authService: AuthenticationService, private candService: CandidatureService, 
    private router: Router, private agentService: AgentService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.authService.loadToken()
    if (this.isLoggedIn) {
     const userfromLS = this.authService.getUser();
     this.user = userfromLS
    }
    this.agentService.getOneAgentByUsername("/agentByUsername/"+this.user.username)
    .subscribe(data=>{
      this.agent=data
      this.authService.isRefresh()
    }, err=>{
      this.authService.logout()
      this.router.navigateByUrl('/login')
    })

/*     $("#menu-toggle").on("click",function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    }); */

    $(window).on("resize",function(e) {
      if(window.innerWidth<=870){
        $("#wrapper").addClass("toggled");
      }else{
        $("#wrapper").removeClass("toggled");
      }
    });
  
  }

  updateProfile(agent:any){
      this.mode=1;
  }
  
  errorMessage:any;

  updateAgent(agent: any){
    this.errorMessage = "";
    this.agentService.updateAgent("/updateAgent",agent)
    .subscribe(data =>{
      console.log("data "+data)
      this.mode=0;
    },(error) =>{
      this.errorMessage = error;
      console.log("err "+this.errorMessage.error.message)
      throw error;
        
    })
    
  }

  backProfile(){
    this.mode=0;
  }

  deleteAgent(idAgent:number){
    this.agentService.deleteAgent("/deleteAgent/"+idAgent)
    .subscribe(data=>{
        this.authService.logout();
        this.router.navigateByUrl('/login')
    },err=>{

    })
  }

}
