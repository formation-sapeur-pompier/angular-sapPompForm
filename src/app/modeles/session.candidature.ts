import { Injectable } from "@angular/core";

@Injectable()
export class SessionCand {
  qualite: string="";
  position=1
  sessionUV : any;
}

@Injectable()
export class SessionUV {
  idSessionUV:any
  startDate=""
  endDate=""
  formateur=null
  typeSessionUV: TypeSessionUV = new TypeSessionUV;
}

@Injectable()
export class TypeSessionUV {
  idTypeSessionUV:any
  title=""
  description=""
  minNumber=""
  maxNumber =""
  listPrerequis: ListPrerequis[]=[]
}

@Injectable()
export class ListPrerequis {
  idPrerequis:any
  title=""
}