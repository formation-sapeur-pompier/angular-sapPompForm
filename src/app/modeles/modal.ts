import { Component, Input, Type } from "@angular/core";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";


@Component({
    selector: 'ngbd-modal-confirm',
    templateUrl: 'template.html'

  })

  export class NgbdModalContent {
    @Input() message:any;
    constructor(public modal: NgbActiveModal) {}
}

/* export class NgbdModalConfirmAutofocus {
    constructor(private _modalService: NgbModal) {}
} */