import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mode:number=0
  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
  forgetPasswordForm:FormGroup= new FormGroup({})
  submitted = false;

  constructor(private authService: AuthenticationService, private router:Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.forgetPasswordForm = this.fb.group({
      username: ['', [Validators.required,Validators.minLength(2)]],
      passwordd: ['', [Validators.required,Validators.minLength(4)]],
    })
  }
  
  get f() { 
    return this.forgetPasswordForm.controls; 
  }

  onLogin(user:any){
    this.authService.login(user)
    .subscribe(resp=>{
      let jwToken = resp.headers.get('authorization')
      if (jwToken!=null) {
        this.authService.saveToken(jwToken);
        this.authService.saveUser(user)
        if (this.authService.isAdmin()) {
          this.router.navigateByUrl('/agents')
        } else {
          this.router.navigateByUrl('/agent')

        }

      }
      

    }, err=>{
      this.mode=1;
      
    })

  }
  onRegister(){
    this.router.navigateByUrl('register')
  }

  passwordForget(){
    $(".row").hide();
    this.mode=2
  }

  forgetPassword(forgetPasswordForm:any){
    this.submitted = true;
    console.log(forgetPasswordForm)
/*     if (this.forgetPasswordForm.invalid) {
      return;
  } */

    this.authService.forgetPassword(forgetPasswordForm)
    .subscribe(data=>{
      window.location.reload();
    },err=>{
      console.log("err")
    })
  }

  onReset() {
    this.submitted = false;
    this.forgetPasswordForm.reset();
}

}
