import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgentService } from '../services/agent.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({})
  submitted = false;
  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
  patternCodepostal="^[0-9]+$"


  constructor(private fb:FormBuilder, private router: Router,
    private agentService: AgentService) { }

  ngOnInit(): void {
    this.registerForm=this.fb.group({
      firstName: ['', [Validators.required,Validators.minLength(2)]],
      lastName: ['', [Validators.required,Validators.minLength(2)]],
      username: ['', [Validators.required,Validators.minLength(2)]],
      email: ['', [Validators.required,Validators.pattern(this.pattern)]],
      password: ['', [Validators.required,Validators.minLength(4)]],
      address: ['', [Validators.required,Validators.minLength(4)]],
      country: ['', [Validators.required,Validators.minLength(4)]],
      city: ['', [Validators.required,Validators.minLength(4)]],
      postalCode: ['', [Validators.required,Validators.minLength(5),Validators.pattern(this.patternCodepostal)]],
    })
  }

  get f() { 
    return this.registerForm.controls; 
  }

  register(registerForm:FormGroup){
    this.submitted = true;

    console.log(registerForm.value)
    if (this.registerForm.invalid) {
        return;
    }
    this.agentService.addAgent("/addAgent",registerForm.value)
    .subscribe(data=>{
      this.router.navigateByUrl("login")
    },err=>{
      console.log(err)
    })
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
