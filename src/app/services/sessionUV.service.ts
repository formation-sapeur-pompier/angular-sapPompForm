import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SessionUV } from "../modeles/session.candidature";
import { AuthenticationService } from "./authentication.service";

@Injectable()
export class SessionUVService{
    private host: string = "http://localhost:8081/api"
    private jwtoken: any;

    constructor(private http: HttpClient, private authService: AuthenticationService){}

    loadToken(){
        if (this.jwtoken==null) {
            this.jwtoken = this.authService.loadToken();
        }
    }

    listNextSessionUV(url:string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }
    getOneSession(url:string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    getAllPrerequis(url: string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    editSessionUV(url:string,session: SessionUV){
        this.loadToken()
        return this.http.put(this.host+url,session,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    addSession(url:string,newSessionuv:SessionUV){
        this.loadToken()
        return this.http.post(this.host+url,newSessionuv,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }
    
}