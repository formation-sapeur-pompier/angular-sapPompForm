import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable()
export class AuthenticationService {


    private host: string = "http://localhost:8081"
    private jwtoken: any;
    private roles: Array<any> = [];
    constructor(private http: HttpClient){

    }

    login(user:any){
        return this.http.post(this.host+"/login",user,{observe:'response'});
    }
    logout() {
        this.jwtoken = null
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        localStorage.removeItem("isRefresh")
        localStorage.removeItem("isAuthenticated")
    }
    saveToken(jwToken:string){
        localStorage.setItem('token',jwToken);
        let jwtHelper = new JwtHelperService();
        this.roles = jwtHelper.decodeToken(jwToken).roles;
        
    }
    public saveUser(user: any): void {
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem("isAuthenticated","true");
    }

    getUser(){
        const user = localStorage.getItem('user');
        if (user) {
            return JSON.parse(user);
          } 
    }

    loadToken(){
        return this.jwtoken = localStorage.getItem('token');
    }

    isAdminApp(){
        this.jwtoken = localStorage.getItem('token')
        let jwtHelper = new JwtHelperService();
        if (this.jwtoken!=null) {
            this.roles = jwtHelper.decodeToken(this.jwtoken).roles;
            for (const r of this.roles) {
                if (r.authority == 'ADMIN') return true;
            }
        }

        return false;
    }

    isAdmin(){
        for (const r of this.roles) {
            if (r.authority == 'ADMIN') return true;
        }
        return false;
    }

    isRefresh(){
        if(localStorage.getItem("isRefresh") == null){
            localStorage.setItem("isRefresh","true");
            window.location.reload();
        }
    }
    isAuthenticated(){
        const isAuth = localStorage.getItem("isAuthenticated")
        if (isAuth=="true") {
            return true;
        }
        return false;
    }
    

    getAgents(){        
        if (this.jwtoken==null) {
            this.loadToken();
            console.log("jwtoken==null "+this.jwtoken)
        }
        console.log("get "+this.jwtoken)
        return this.http.get(this.host+"/api/agents",{headers: new HttpHeaders({'Authorization':this.jwtoken})});
        
    }
    getCandidatureByQualiteAgent(url: string) {
        if (this.jwtoken==null) {
            this.loadToken();
        }
        console.log("get "+this.jwtoken)
        return this.http.get(this.host+"/api/"+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})});
    }

    forgetPassword(agent:any){
        if (this.jwtoken==null) {
            this.loadToken();
        }
        return this.http.post(this.host+"/api/updatePassword",agent,{observe:'response'});
    }

}