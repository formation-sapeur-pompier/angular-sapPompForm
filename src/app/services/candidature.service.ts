import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SessionCand } from "../modeles/session.candidature";
import { AuthenticationService } from "./authentication.service";

@Injectable()
export class CandidatureService{
    private host: string = "http://localhost:8081/api"
    private jwtoken: any;
    constructor(private http: HttpClient, private authService: AuthenticationService){

    }

    loadToken(){
        if (this.jwtoken==null) {
            this.jwtoken = this.authService.loadToken();
        }
    }

    updateStatus(url:any){
       this.loadToken()
        return this.http.put(this.host+"/updateStatus/"+url,null,{headers: new HttpHeaders({'Authorization':this.jwtoken})});
    }

    getCandidatureByQualiteAndStatutAgent(url: string) {
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})});
    }

    getCandidatureByAgentAndQualiteAndStatut(url:string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})}); 
    }

    getOneCandidatureByAgentUsername(url:string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    getOneCandidatureById(url:string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    addCandidature(url:string, sessionCand:SessionCand){
        this.loadToken()
        return this.http.post(this.host+url,sessionCand,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    allCandidatures(url:string){
        this.loadToken();
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }
}