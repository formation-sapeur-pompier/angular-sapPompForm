import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthenticationService } from "./authentication.service";

@Injectable()
export class AgentService{
    private host: string = "http://localhost:8081/api"
    private jwtoken: any;
    constructor(private http: HttpClient, private authService: AuthenticationService){}

    loadToken(){
        if (this.jwtoken==null) {
            this.jwtoken = this.authService.loadToken();
        }
    }
    getAllAgents(url:string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    getOneAgent(url:string){
        this.loadToken()
        return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    getOneAgentByUsername(url:string){
        this.loadToken()
    return this.http.get(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    updateAgent(url:string,agent:any){
        this.loadToken()
        return this.http.put(this.host+url,agent,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }

    deleteAgent(url:string){
        this.loadToken();
        return this.http.delete(this.host+url,{headers: new HttpHeaders({'Authorization':this.jwtoken})})
    }


    addAgent(url:string, agent:any) {
        this.loadToken()
        return this.http.post(this.host+url,agent)
    }
}