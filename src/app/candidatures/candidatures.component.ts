import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { CandidatureService } from '../services/candidature.service';

@Component({
  selector: 'app-candidatures',
  templateUrl: './candidatures.component.html',
  styleUrls: ['./candidatures.component.css']
})
export class CandidaturesComponent implements OnInit {
  candidatures:any
  candidat:any;
  qualite:string="";
  username:string=""
  mode=0
  stagiaire=false;
  formateur=false;
  currentStatut:any;
  currentQualite:any;

  confirmationTitle = 'Confirmation';
  confirmationMessage = 'Êtes-vous sûr de vouloir confirmer';
  confirmClicked = false;
  cancelClicked = false

  cancelText='Annuler';
  acceptText='Accepter'
  refuserText='Refuser'

  refuserTitle = 'Confirmation';
  refuserMessage = 'Êtes-vous sûr de vouloir refuser';



  constructor(private candService:CandidatureService, private route:ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.candService.getOneCandidatureByAgentUsername("/candidatures").pipe(
      map((data:any)=> data.filter((c:any)=>c.qualite==='STAGIAIRE')
    )
    )
     .subscribe(data=>{
       if (data.length!=0) {
         this.stagiaire=true;
         this.qualite="stagiaire"
         this.candidatures=data;
         this.currentStatut="EN ATTENTE"
       } else {
         this.candService.allCandidatures("/candidatures").pipe(
           map((data:any)=> data.filter((c:any)=>c.qualite==='FORMATEUR')
         )
         ).subscribe(data=>{
           this.formateur=true;
           this.qualite="formateur"
           this.candidatures=data;
           this.currentStatut="EN ATTENTE"
         },err=>{
           console.log(err)
         })
       }

       this.candService.allCandidatures("/candidatures").pipe(
         map((data:any)=> data.filter((c:any)=>c.qualite==='FORMATEUR')
       )
       ).subscribe(data=>{
         if (data.length!=0) {
           this.formateur=true;
         }
         
       },err=>{
         console.log(err)
       })

     }, err=>{
       console.log(err)
     })
  }

  getQualite(currentStatut:string,qualite:string){
    this.candService.allCandidatures("/candidatures").pipe(
      map((data:any)=> data.filter((c:any)=>(c.qualite===qualite && c.statut===currentStatut))
    )
    ).subscribe(data=>{
      this.qualite=qualite.toLowerCase()
      this.candidatures=data
      this.currentStatut=currentStatut
    },err=>{
      console.log(err)
    })
  }

  getOneCand(idCandidature:number){
    this.candService.getOneCandidatureById("/oneCandidature/"+idCandidature)
    .subscribe(data=>{
      this.candidat=data
      this.mode=1
    },err=>{
      console.log(err)
    })
  }

  golistCand(qualite:string){
    this.qualite=qualite.toLowerCase()
    this.mode=0
  }

  acceptCand(idCandidature:number){
    this.candService.updateStatus(idCandidature+"/ACCEPTE")
    .subscribe(r=>{
      this.mode=1
      window.location.reload();
    },err=>{
      console.log(err)
    })
    
  }

  rejectCand(idCandidature:number){
    this.candService.updateStatus(idCandidature+"/REFUSE")
    .subscribe(r=>{
      this.mode=1
      window.location.reload();
    },err=>{
      console.log(err)
    })
  }

  getStatut(qualite: string,statut: string){
    this.candService.getCandidatureByQualiteAndStatutAgent("/candidatureByQualiteAndStatut/"+qualite+"/"+statut)
    .subscribe(data=>{
      this.candidatures=data
      this.currentQualite=qualite;
      this.currentStatut=statut
    }, err=>{
      console.log(err)
    })
  }

}
