import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { CandidatureService } from '../services/candidature.service';
import * as $ from 'jquery';
import { AgentService } from '../services/agent.service';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.css']
})
export class AgentsComponent implements OnInit {
  agents:any; 
  agent:any;
  isLoggedIn = false;
  username: string="";
  currentQualite:any;
  mode:number=0;
  currentStatut:any;

  constructor(private authService: AuthenticationService, private router:Router, 
    private candService: CandidatureService, private agentsService: AgentService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.authService.loadToken()
    if (this.isLoggedIn) {
     const user = this.authService.getUser();
     this.username = user.username
    }
    this.agentsService.getAllAgents("/agents")
      .subscribe(data=>{
      this.agents=data
      this.authService.isRefresh()
      if(localStorage.getItem("isRefresh") == null){
        localStorage.setItem("isRefresh","true");
        window.location.reload();
      }
      
    }, err=>{
      console.log("err "+err)
      this.authService.logout()
      this.router.navigateByUrl('/login')
    })


    $(window).on("resize",function(e) {
      if(window.innerWidth<=870){
        $("#wrapper").addClass("toggled");
      }else{
        $("#wrapper").removeClass("toggled");
      }
    });

  }

  getOneAgent(idAgent:number){
    this.agentsService.getOneAgent("/agent?idAgent="+idAgent)
    .subscribe(data=>{
      this.agent= data
      this.mode=1
    }, err=>{
      console.log(err)
    })
  }

  golistAgents(){
    this.mode=0
  }


  getQualite(qualite: string, statut:string){
    this.candService.getCandidatureByQualiteAndStatutAgent("candidatureByQualiteAndStatut/"+qualite+"/"+statut)
    ?.subscribe(data=>{
      this.agents=data
      this.currentQualite=qualite;
      this.currentStatut=statut;
      /*console.log("ls "+localStorage.getItem("isRefresh"))
      if(localStorage.getItem("isRefresh") == null){
        localStorage.setItem("isRefresh","true");
        window.location.reload();
      }*/
      
    }, err=>{
      this.authService.logout()
      this.router.navigateByUrl('/login')
    })
  }

  getStatut(qualite: string,statut: string){
    this.candService.getCandidatureByQualiteAndStatutAgent("candidatureByQualiteAndStatut/"+qualite+"/"+statut)
    ?.subscribe(data=>{
      this.agents=data
      this.currentQualite=qualite;
      this.currentStatut=statut
      /*console.log("ls "+localStorage.getItem("isRefresh"))
      if(localStorage.getItem("isRefresh") == null){
        localStorage.setItem("isRefresh","true");
        window.location.reload();
      }*/
      
    }, err=>{
      this.authService.logout()
      this.router.navigateByUrl('/login')
    })
  }


  acceptCand(idCandidature:number){
    this.candService.updateStatus(idCandidature+"/ACCEPTE")
    .subscribe(r=>{
      this.mode=1
      window.location.reload();
    },err=>{
      console.log(err)
    })
    
  }

  rejectCand(idCandidature:number){
    this.candService.updateStatus(idCandidature+"/REFUSE")
    .subscribe(r=>{
      this.mode=1
      window.location.reload();
    },err=>{
      console.log(err)
    })
  }
  candByAgent(idAgent:number,qualite:string,statut:string){
    this.candService.getCandidatureByAgentAndQualiteAndStatut("listCandidatureByAgentAndQualiteAndStatut/"+idAgent+"/"+qualite+"/"+statut)
    .subscribe(data=>{
      this.mode=1
      this.agents=data
      this.currentStatut=statut
      /*if(localStorage.getItem("isRefresh") == null){
        localStorage.setItem("isRefresh","true");
        window.location.reload();
      }*/
      
    }, err=>{
      this.authService.logout()
      this.router.navigateByUrl('/login')
    })
  }

  return(){
    this.mode=0;
  }

}
