import { Component, Injectable, OnInit, Type } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ListPrerequis, SessionCand, SessionUV } from '../modeles/session.candidature';
import { AgentService } from '../services/agent.service';
import { AuthenticationService } from '../services/authentication.service';
import { SessionUVService } from '../services/sessionUV.service';
import * as $ from 'jquery';
import { CandidatureService } from '../services/candidature.service';
import { from } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from '../modeles/modal';



@Component({
  selector: 'app-session-uv',
  templateUrl: './session-uv.component.html',
  styleUrls: ['./session-uv.component.css']
})


export class SessionUVComponent implements OnInit {
  isLoggedIn = false;
  user: any;
  sessionUV: any;
  oneSession: any;
  agent:any;
  mode=0;
  isShow=true;
  sessionCand = new SessionCand();
  certifObtenus:any;
  isStagiaire=false;
  isFormateur=false
  quality:any;
  stagiaire="STAGIAIRE";
  formateur="FORMATEUR";
  listFormateur:any
  listPrerequis:ListPrerequis[]=[]
  sessionuvEdit= new SessionUV()
  selectedObject : ListPrerequis =new ListPrerequis();

  
  


  constructor(private authService: AuthenticationService, private agentService:AgentService, 
    private router: Router, private sessionService: SessionUVService, private candService:CandidatureService,
    private modalService: NgbModal ) { }

    
  ngOnInit(): void {
    this.sessionService.listNextSessionUV("/nextSessionUV")
    .subscribe((data:any)=>{
      if (data.length!=0) {
        this.isStagiaire=true;
        this.quality="stagiaire";
      }
      this.sessionUV=data;
      this.sessionService.listNextSessionUV("/nextSessionUV").pipe(
        map((data:any)=> data.filter((f:any)=>f.formateur==null)
      )
      ).subscribe(data=>{
        if (data.length!=0) {
          this.isFormateur=true;
        }
      })
      
    }, err=>{
      console.log(err)
    })

    this.isLoggedIn = !!this.authService.loadToken()
    if (this.isLoggedIn) {
     const userfromLS = this.authService.getUser();
     this.user = userfromLS
    }


    this.agentService.getOneAgent("/agentByUsername/"+this.user.username)
    .subscribe((data:any)=>{
      this.agent=data
      from([data]).pipe(map((a:any)=>a.certifObtenus))
       .subscribe(certif=>{
          this.certifObtenus = certif.map((c:any)=>c.title)
        },err=>{
          console.log(err)
        })
    }, err=>{
      console.log(err)
    })

    
      this.sessionService.getAllPrerequis("/prerequis")
      .subscribe((data:any)=>{
        this.listPrerequis=data
      },err=>{
        console.log(err)
      })

/*       this.candService.allCandidatures("/candidatures").pipe(
        map((data:any)=> data.filter((c:any)=>c.qualite==='FORMATEUR')
      )
      ).subscribe(data=>{
        this.listFormateur=data
      },err=>{
        console.log(err)
      }) */

    $(window).on("resize",function(e) {
      if(window.innerWidth<=870){
        $("#wrapper").addClass("toggled");
      }else{
        $("#wrapper").removeClass("toggled");
      }
    });

  }

  getOneSessionUV(idSessionUV:number,quality:string){
    this.sessionService.getOneSession("/oneSessionUV/"+idSessionUV)
    .subscribe(data=>{
      this.oneSession=data
      this.mode=1
      this.quality=quality
    },err=>{
      console.log(err)
    })
  }
  golistSession(){
    this.mode=0
  }

  onQualiteCand(oneSession:any){
    this.isShow=true;
  }


  onCandSession(oneSession:any,form: NgForm,agent:any) {
    this.isShow=false
    this.sessionCand.qualite= form.value.qualite;
    this.sessionCand.sessionUV=oneSession;
    this.candService.addCandidature("/candidature?idAgent="+agent.idAgent,this.sessionCand)
    .subscribe(data=>{
      this.router.navigateByUrl("agent/candidature/"+agent.username)
    }, err=>{
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.message = err.error.message;
      this.router.navigateByUrl("agent/candidature/"+agent.username)
      console.log(err)
    })
  }


  getQualite(qualite:string){
    this.mode=0
    if (qualite=='formateur') {
      this.sessionService.listNextSessionUV("/nextSessionUV").pipe(
        map((data:any)=> data.filter((f:any)=>f.formateur==null)
      )
      ).subscribe(data=>{
        if (data.length!=0) {
          this.quality="formateur";
          this.sessionUV=data;
        }
      },err=>{
  
      })

    } else {
      this.sessionService.listNextSessionUV("/nextSessionUV")
      .subscribe(data=>{
      
        this.quality="stagiaire";
        this.sessionUV=data;
    },err=>{

    })
    }

  }

  isAdmin(){
    return this.authService.isAdmin();
  }

  update(idSessionUV:number){
    this.sessionService.getOneSession("/oneSessionUV/"+idSessionUV)
    .subscribe(data=>{
      this.sessionUV=data
      this.mode=2

      this.candService.allCandidatures("/candidatures").pipe(
        map((data:any)=> data.filter((c:any)=>(c.qualite==='FORMATEUR')&&(c.sessionUV.typeSessionUV.title===this.sessionUV.typeSessionUV.title))
      )
      ).subscribe(data=>{
        this.listFormateur=data
      },err=>{
        console.log(err)
      })

    },err=> {
      console.log(err)
    })
  }

  updateSession(sessionForm: NgForm){

     console.log(sessionForm.value)
     this.sessionuvEdit.idSessionUV= sessionForm.value.idSessionUV
     this.sessionuvEdit.formateur=sessionForm.value.formateur
     this.sessionuvEdit.startDate= sessionForm.value.startDate
     this.sessionuvEdit.endDate= sessionForm.value.endDate
     this.sessionuvEdit.typeSessionUV.title= sessionForm.value.title
     this.sessionuvEdit.typeSessionUV.idTypeSessionUV= sessionForm.value.idTypeSessionUV
     this.sessionuvEdit.typeSessionUV.description= sessionForm.value.description
     this.sessionuvEdit.typeSessionUV.minNumber= sessionForm.value.minNumber
     this.sessionuvEdit.typeSessionUV.maxNumber= sessionForm.value.maxNumber
     this.sessionuvEdit.typeSessionUV.listPrerequis= sessionForm.value.listPrerequis

     
     this.sessionService.editSessionUV("/updateSessionUV",this.sessionuvEdit)
     .subscribe(data=>{
      window.location.reload();
     },err=>{
       console.log(err)
     })
     
  }

  goNewSession(){
    this.router.navigateByUrl('newSessionuv')
  }

  goBack(){
    window.location.reload();
  }
  checkPrerequisAndCertif(certifObtenus:any, typeSessionUV:any){
    const titles :any[]=[]
    for (const l in typeSessionUV.listPrerequis) {
      
        const title = typeSessionUV.listPrerequis[l].title;
        titles.push(title)
      
    }
    return (certifObtenus.join()==titles.join())|| !certifObtenus.includes(typeSessionUV.title)? true:false
  }
}
