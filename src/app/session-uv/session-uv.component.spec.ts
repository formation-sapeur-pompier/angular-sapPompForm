import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionUVComponent } from './session-uv.component';

describe('SessionUVComponent', () => {
  let component: SessionUVComponent;
  let fixture: ComponentFixture<SessionUVComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionUVComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionUVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
