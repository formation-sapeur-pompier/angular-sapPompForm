import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AgentsComponent } from './agents/agents.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from './services/authentication.service';
import { CandidatureService } from './services/candidature.service';
import { AgentComponent } from './agent/agent.component';
import { AgentService } from './services/agent.service';
import { CandidatureComponent } from './candidature/candidature.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { SessionUVComponent } from './session-uv/session-uv.component';
import { SessionUVService } from './services/sessionUV.service';
import { CandidaturesComponent } from './candidatures/candidatures.component';
import { NewSessionUVComponent } from './new-session-uv/new-session-uv.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AgentsComponent,
    AgentComponent,
    CandidatureComponent,
    SessionUVComponent,
    CandidaturesComponent,
    NewSessionUVComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ConfirmationPopoverModule.forRoot({
      focusButton: 'confirm',
    })
  ],
  providers: [AuthenticationService,CandidatureService, AgentService, SessionUVService],
  bootstrap: [AppComponent]
})
export class AppModule { }
