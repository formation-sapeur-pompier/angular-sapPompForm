import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import * as $ from 'jquery';
import { LoginComponent } from './login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-sapPompForm';
  isLoggedIn = false;
  username: string="";
  collapse: boolean = true;
  currentSideBar:any;

  constructor( 
    private authService:AuthenticationService, 
    private router:Router,
    private route: ActivatedRoute)
    {}

  ngOnInit(): void {
   this.isLoggedIn = !!this.authService.loadToken()
   if (this.isLoggedIn) {
    const user = this.authService.getUser();
    this.username = user.username
   }

   if (this.authService.isAdminApp()) {
    this.currentSideBar="agents";
   }else {
    this.currentSideBar="profile";
   }
   

   /* ************* sidebar toogle **************** */
/*    $("#menu-toggle").on("click",function(e) {
    console.log("rrr")
    e.preventDefault();
    console.log("rrr")

    $("#wrappers").toggleClass("toggled");
  }); */

  $(window).on("resize",function(e) {
      if(window.innerWidth<=870){
        $("#wrappers").addClass("toggled");
      }else{
        $("#wrappers").removeClass("toggled");
    }
  });

  $(window).on("resize",function(e) {
    if(window.innerWidth<=870){
      $("#wrappes").addClass("toggled");
    }else{
      $("#wrappes").removeClass("toggled");
  }
});
  
  /* ************************************************* */

  /* ************* active side bar from url **************** */
  this.router.events.subscribe((val)=>{
    if (val instanceof NavigationEnd) {
      const url = val.url.split('/')[2];
      if (url=="candidature") {
        this.currentSideBar="candidature";
      }else if(url=="sessionUV" || val.url=="/newSessionuv") {
        this.currentSideBar="sessionUV";
    }else if(val.url=="/agents") {
          this.currentSideBar="agents";
    } else if(val.url=="/candidatures") {
      this.currentSideBar="candidatures";
      }else {
        this.currentSideBar="profile";
    }
    }
    
  })
  /* ************************************************* */


  }

  toggle(){
    console.log("eeee")
    $("#wrappers").toggleClass("toggled");
    $("#wrappes").toggleClass("toggled");
    $("#wrapper").toggleClass("toggled");
  }

/*   show(){
    $(".dropdown-menu").toggle();
  } */
  isAuthenticated(){
    return this.authService.isAuthenticated();
  }

  isAdmin(){
    //console.log(this.authService.isAdminApp())
    return this.authService.isAdminApp()
  }

  onCandidadure(username:string){
    this.router.navigateByUrl("agent/candidature/"+username)
    this.currentSideBar="candidature";
    $("#wrappers").removeClass("toggled");
  }
  
  onListCandStagiaire(username:string){
    this.router.navigateByUrl("agent/candidature/"+username)
    this.currentSideBar="candidature";
    $("#wrappers").removeClass("toggled");
  }

  onListCandFormateur(username:string){
    this.router.navigateByUrl("agent/candidature/"+username)
    this.currentSideBar="candidature";
    $("#wrappers").removeClass("toggled");
  }

  onProfile(){
    this.router.navigateByUrl("agent")
    this.currentSideBar="profile";
    $("#wrappers").removeClass("toggled");
  }

  onSessionUV() {
    this.router.navigateByUrl("agent/sessionUV")
    this.currentSideBar="sessionUV";
    $("#wrappers").removeClass("toggled");
  }

  onAgents(){
    this.router.navigateByUrl("agents")
    this.currentSideBar="agents";
    $("#wrappers").removeClass("toggled");
  }
  onCandidatures(){
    this.router.navigateByUrl("candidatures")
    this.currentSideBar="candidatures";
    $("#wrappers").removeClass("toggled");
  }  

  logout(){
    this.authService.logout()
    this.router.navigateByUrl('/login')
    window.location.reload();
  }

}
