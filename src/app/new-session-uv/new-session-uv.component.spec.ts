import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSessionUVComponent } from './new-session-uv.component';

describe('NewSessionUVComponent', () => {
  let component: NewSessionUVComponent;
  let fixture: ComponentFixture<NewSessionUVComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewSessionUVComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSessionUVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
