import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { ListPrerequis, SessionUV } from '../modeles/session.candidature';
import { SessionUVService } from '../services/sessionUV.service';

@Component({
  selector: 'app-new-session-uv',
  templateUrl: './new-session-uv.component.html',
  styleUrls: ['./new-session-uv.component.css']
})
export class NewSessionUVComponent implements OnInit {
  listPrerequis: any
  sessionForm: FormGroup = new FormGroup({});
  submitted = false;
  newSessionuv= new SessionUV()

  datePattern=/^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/

  constructor(public fb: FormBuilder,private sessionService: SessionUVService, private router:Router) { }

  ngOnInit(): void {
    this.sessionForm = this.fb.group({
      title:['',Validators.required],
      formateur:[],
      startDate:['', [Validators.required,Validators.pattern(this.datePattern)]],
      endDate:['',[Validators.required,Validators.pattern(this.datePattern)]],
      description:['',Validators.required],
      listPrerequis:[],
      minNumber:[Validators.required,Validators.min(1)],
      maxNumber:[Validators.required,Validators.min(1)]
    })

    this.sessionService.getAllPrerequis("/prerequis")
    .subscribe((data:any)=>{
      this.listPrerequis=data
      
    },err=>{
      console.log(err)
    })

    $(window).on("resize",function(e) {
      if(window.innerWidth<=870){
        $("#wrapper").addClass("toggled");
      }else{
        $("#wrapper").removeClass("toggled");
      }
    });
  }

  get f() { 
    return this.sessionForm.controls; 
  }

  addNewSessionUV(sessionForm: FormGroup){
    this.submitted = true;

    //console.log(sessionForm)
     
    if (this.sessionForm.invalid) {

      return;
    }

    this.newSessionuv.formateur=sessionForm.value.formateur
    this.newSessionuv.startDate= sessionForm.value.startDate
    this.newSessionuv.endDate= sessionForm.value.endDate
    this.newSessionuv.typeSessionUV.title= sessionForm.value.title
    this.newSessionuv.typeSessionUV.description= sessionForm.value.description
    this.newSessionuv.typeSessionUV.minNumber= sessionForm.value.minNumber
    this.newSessionuv.typeSessionUV.maxNumber= sessionForm.value.maxNumber
    this.newSessionuv.typeSessionUV.listPrerequis= sessionForm.value.listPrerequis
    //console.log(this.newSessionuv)
    
    this.sessionService.addSession("/sessionUV",this.newSessionuv)
    .subscribe(data=>{
      this.router.navigateByUrl('agent/sessionUV')
    }, err=>{
      console.log(err)
    })
  }

  onReset(){
    this.submitted = false;
    this.sessionForm.reset();
    this.router.navigateByUrl('agent/sessionUV')
  }

}
