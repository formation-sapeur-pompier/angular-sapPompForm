import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidatureService } from '../services/candidature.service';
import { map, catchError } from 'rxjs/operators';
import * as $ from 'jquery';

@Component({
  selector: 'app-candidature',
  templateUrl: './candidature.component.html',
  styleUrls: ['./candidature.component.css']
})
export class CandidatureComponent implements OnInit {
  candidature:any[]=[];
  candidat:any;
  qualite:string="";
  username:string=""
  mode=0
  stagiaire=false;
  formateur=false

  ca:any
  b:any[]=[]
  constructor(private candService:CandidatureService, private route:ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.username = this.route.snapshot.params.username;
    console.log("user "+this.route.snapshot.params.username)
      this.candService.getOneCandidatureByAgentUsername("/oneCandidatureByAgentUsername/"+this.username).pipe(
       map((data:any)=> data.filter((c:any)=>c.qualite==='STAGIAIRE')
     )
     )
      .subscribe(data=>{
        if (data.length!=0) {
          this.stagiaire=true;
          this.qualite="stagiaire"
          this.candidature=data;
        } else {
          this.candService.getOneCandidatureByAgentUsername("/oneCandidatureByAgentUsername/"+this.username).pipe(
            map((data:any)=> data.filter((c:any)=>c.qualite==='FORMATEUR')
          )
          ).subscribe(data=>{
            this.formateur=true;
            this.qualite="formateur"
            this.candidature=data;
          },err=>{
            console.log(err)
          })
        }

        this.candService.getOneCandidatureByAgentUsername("/oneCandidatureByAgentUsername/"+this.username).pipe(
          map((data:any)=> data.filter((c:any)=>c.qualite==='FORMATEUR')
        )
        ).subscribe(data=>{
          if (data.length!=0) {
            this.formateur=true;
          }
          
        },err=>{
          console.log(err)
        })

      }, err=>{
        console.log(err)
      })
 
/*       $("#menu-toggle").on("click",function(e) {
        console.log("eee")
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      }); */
      //$("#wrapper").removeClass("toggled");
      $(window).on("resize",function(e) {
        if(window.innerWidth<=870){
          $("#wrapper").addClass("toggled");
        }else{
          $("#wrapper").removeClass("toggled");
        }
      });

  }

  getQualite(username:string,qualite:string){
    this.candService.getOneCandidatureByAgentUsername("/oneCandidatureByAgentUsername/"+username).pipe(
      map((data:any)=> data.filter((c:any)=>c.qualite===qualite)
    )
    ).subscribe(data=>{
      this.qualite=qualite.toLowerCase()
      this.candidature=data
    },err=>{
      console.log(err)
    })
  }
  getOneCand(idCandidature:number){
    this.candService.getOneCandidatureById("/oneCandidature/"+idCandidature)
    .subscribe(data=>{
      this.candidat=data
      this.mode=1
    },err=>{
      console.log(err)
    })
  }

  golistCand(qualite:string){
    this.qualite=qualite.toLowerCase()
    this.mode=0
  }

}
