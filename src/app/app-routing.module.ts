import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgentComponent } from './agent/agent.component';
import { AgentsComponent } from './agents/agents.component';
import { CandidatureComponent } from './candidature/candidature.component';
import { CandidaturesComponent } from './candidatures/candidatures.component';
import { LoginComponent } from './login/login.component';
import { NewSessionUVComponent } from './new-session-uv/new-session-uv.component';
import { RegisterComponent } from './register/register.component';
import { SessionUVComponent } from './session-uv/session-uv.component';

const routes: Routes = [
  { path: "register" , component: RegisterComponent},
  { path: "login", component: LoginComponent},
  { path: "agents", component: AgentsComponent},
  { path: "agent", component: AgentComponent},
  { path: ":param/candidature/:username", component: CandidatureComponent},
  { path:"agent/sessionUV", component: SessionUVComponent},
  { path: "candidatures", component: CandidaturesComponent},
  { path: "newSessionuv", component: NewSessionUVComponent},
  { path: "", redirectTo: "/login", pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
